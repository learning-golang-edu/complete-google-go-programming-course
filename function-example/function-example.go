package main

import "fmt"

func max(i int, j int) {
	if i > j {
		fmt.Println(i)
	} else {
		fmt.Println(j)
	}
}

func min(i int, j int) int {
	if i < j {
		return i
	} else {
		return j
	}
}

func maxv(i int, j int, k int) {
	if i > j {
		k = i
	} else {
		k = j
	}
}

func maxp(i int, j int, k *int) {
	if i > j {
		*k = i
	} else {
		*k = j
	}
}

func main() {
	max(10, 15)
	fmt.Println("min:", min(100, 15))
	var c int
	maxv(100, 15, c)
	fmt.Println("c =", c)
	maxp(100, 15, &c)
	fmt.Println("c =", c)
}
