package main

import "fmt"

const gCap = 5 // total capacity of our list
var gGroceries [gCap]string
var gLen = 0 // total number of grocery items in our current array

var gGroceriesSlice []string

func addGrocery(a string) {
	if gLen < gCap {
		gGroceries[gLen] = a
		gLen++
	} else {
		fmt.Println("Too many items, now we are done for!!!")
	}
}

func addGroceryToSlice(a ...string) {
	fmt.Println("Capacity", cap(gGroceriesSlice))
	for _, item := range a {
		gGroceriesSlice = append(gGroceriesSlice, item)
	}
}

func listGroceries() {
	fmt.Println("Grocery list is as following:")
	for i := 0; i < gLen; i++ {
		fmt.Println(gGroceries[i])
	}
}

func listGroceriesSlice() {
	fmt.Println("Grocery list is as following:")
	for i := 0; i < len(gGroceriesSlice); i++ {
		fmt.Println(gGroceriesSlice[i])
	}
}

func listGroceriesSliceAdvanced() {
	fmt.Println("Grocery list is as following:")
	for _, item := range gGroceriesSlice {
		fmt.Println(item)
	}
}

func main() {
	fmt.Println("======= Array example =======")
	addGrocery("Bread")
	addGrocery("Cucumbers")
	addGrocery("Salt and Vinegar Potato Chips")
	addGrocery("Fruit Cake")
	addGrocery("Pokemon Game")
	addGrocery("Ice Cream")
	addGrocery("Holiday Ice Cream")
	listGroceries()

	fmt.Println("======= Slice example =======")
	addGroceryToSlice("Bread")
	addGroceryToSlice("Cucumbers")
	addGroceryToSlice("Salt and Vinegar Potato Chips")
	addGroceryToSlice("Fruit Cake")
	addGroceryToSlice("Pokemon Game")
	addGroceryToSlice("Ice Cream")
	addGroceryToSlice("Holiday Ice Cream", "Apples", "Honey")
	listGroceriesSlice()

	fmt.Println("======= Advanced slice example =======")
	listGroceriesSliceAdvanced()
}
